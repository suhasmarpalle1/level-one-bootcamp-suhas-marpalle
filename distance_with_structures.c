//WAP to find the distance between two points using structures and 4 functions.
#include<stdio.h>
#include<math.h>
struct distance_points
{  
   float x,y; 
};
float distance_find(struct distance_points a,struct distance_points b)
{ float answer;
   answer=sqrt((a.x-b.x)*(a.x-b.x)+(a.y-b.y)*(a.y-b.y));
   return answer;
}
int main()
{ 
   struct distance_points a,b;
 printf("\nEnter the coordinates of A point:\n");
   printf("\nX-Axis coordinate: ");
 scanf("%f",&a.x);
   printf("\nY-Axis coordinate: ");
scanf("%f",&a.y);
   printf("\nEnter the coordinates of B point:\n");
 printf("\nX-Axis coordinate: ");
   scanf("%f",&b.x);
printf("\nY-Axis coordinate: ");
   scanf("%f",&b.y);

   printf("Distance between points A and B:%f\n",distance_find(a,b));
   return 0;
}

